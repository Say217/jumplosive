extends Label

func _process(delta):
	if(global.lives != 0):
		self.text = "Still Alive"
	else:
		self.text = "Dead"
