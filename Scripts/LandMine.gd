extends Area2D

export (String) var sceneName = "Level 1"

onready var animator = self.get_node("AnimationPlayer")
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	animator.play("idle")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_LandMine_body_entered(body):
	if body.get_name() == "Player":
		global.lives -= 1
		body.is_alive = false
		body.speed = 0
		body.get_node("Sprite").hide()
		body.get_node("Sprite2").show()
		animator.play("Explosion")

func gameOver():
	get_tree().change_scene(str("res://Scenes/Game Over.tscn"))
