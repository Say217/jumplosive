extends KinematicBody2D

export (int) var speed = 200
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600
export (bool) var is_alive = true

const UP = Vector2(0,-1)

var velocity = Vector2()


onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")
	
func get_input():
	velocity.x = speed
	if is_on_floor() and Input.is_action_just_pressed('jump') and is_alive:
		velocity.y = jump_speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		animator.play("Jump")
	elif velocity.x != 0:
		animator.play("Walk")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	else:
		animator.play("Idle")
	
